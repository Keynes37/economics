---
title: "Introducción Economía de la Salud"
session: "01"
subtitle: "Universidad del Norte"
author: Carlos Yanes Guerra
date: "`r Sys.Date()`"
output:
  xaringan::moon_reader:
    css: ["default", "assets/css/my-theme.css", "assets/css/my-fonts.css", "assets/css/u-tilizable.css"]
    seal: false 
    lib_dir: libs
    nature:
      highlightStyle: dracula
      highlightLanguage: ["r", "yaml", "markdown"]
      slideNumberFormat: "" 
      highlightLines: true
      countIncrementalSlides: false
      ratio: "16:9"
      beforeInit: "https://platform.twitter.com/widgets.js"
    includes:
      in_header: assets/header.html  
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
episode_counter <- 0
# Knitr options
knitr::opts_chunk$set(
  comment = "#>",
  fig.align = "center",
  fig.height = 7,
  fig.width = 10.5,
  warning = F,
  message = F
)
```

```{r packages, include=FALSE}
library(countdown)
library(ymlthis)
library(tidyverse)
library(fontawesome)
library(flextable)
library(babynames)
library(DiagrammeR)
library(hrbrthemes)
library(econocharts)
library(plotly)

ggplot2::theme_set(ggplot2::theme_minimal())
```

class: title-slide, center, middle

<span class="fa-stack fa-4x">
  <i class="fa fa-circle fa-stack-2x" style="color: #ffffffcc;"></i>
  <strong class="fa-stack-1x" style="color:#3b4245;">`r rmarkdown::metadata$session`</strong>
</span> 

# `r rmarkdown::metadata$title`

## `r rmarkdown::metadata$subtitle`

### `r rmarkdown::metadata$author` &#183; IEEC

#### [Departamento de Economía](https://www.uninorte.edu.co/web/departamento-de-economia)


---
background-image: url(images/hola.png)
background-position: top center
background-size: 80%
class: bottom, center

.pull-left[
### Carlos Yanes Guerra
`r fa('graduation-cap')` Profesor Asistente (Econometría, Microeconomía y algo de Ciencia de Datos) en el Departamento de Economía .RRed[Bloque D]
<br>
[<i class="fab fa-github"></i> @keynes37](https://github.com/keynes37)  
[<i class="fab fa-twitter"></i> @keynes37](https://twitter.com/keynes37)
`r fa('envelope')`[cayanes@uninorte.edu.co](mailto:cayanes@uninorte.edu.co)
]

---
class: middle, center

# <i class="fas fa-cloud"></i>

# Recuerde que los elementos del curso estan en:

<https://cursos.uninorte.edu.co/d2l/home/28279>

---
# Respecto a notas y actividades

--

```{r table01, echo=FALSE}
library(kableExtra)
text_tbl <- data.frame(
  Items = c("Participación Activa", "Quizzes", "Talleres","Control de lectura", " Examen Final"),
  Condicion = c(
    "En las sesiones",
    "Mitad de jornada",
    "Hasta la siguiente semana", 
    "Inter-sesiones",
    "Semana Final: Abril 23"
  ),
  Porcentaje = c ("10%","20%","20%","20%", "30%")
)

kbl(text_tbl) %>%
  kable_paper(full_width = F) %>%
  column_spec(1, bold = T, color = "black", border_right = T) %>%
  column_spec(2, width = "30em", background = "yellow") %>%
  column_spec(3, bold = F, color = "black", border_left = T)
```


---

class: middle, center

| Hora          | Actividad                               |
|:--------------|:----------------------------------------|
| 14:30 - 14:45 | Presentación participantes              |
| 14:45 - 15:50 | Sesión 1 (Introducción)                 |
| 15:50 - 16:05 | *Break* `r emo::ji("coffee")`           |
| 16:05 - 16:45 | Sesión 2 (Actividad 1)                  |
| 16:45 - 17:15 | Discusión (Actividad 1)                 |
| 17:15 - 17:30 | *Break* `r emo::ji("tea")`              |
| 17:30 - 18:15 | Sesión 3 (Contextualizando)             |

---
class: middle, center

# <i class="fas fa-home"></i>

## Lo importante de la sesión siempre es aprender

### Pregunte todo lo que le parezca extraño

---
layout: true

<div class="my-footer"><span>Economía de la Salud | CAYG | www.uninorte.edu.co </span></div>


---
# Empecemos

--

"... _¡Hubo 63 leyes y actos legislativos, 22 decretos y 8 sentencias de la Corte Constitucional!_

_No es de extrañar, entonces, que el mercado laboral en Colombia funcione mal, que el desempleo sea tan alto, que la protección social sea tan deficiente, que los trabajadores informales no puedan pensionarse, que la productividad sea negativa y que la población se sienta mal tratada..._ " 

.RRed[Carlos Caballero Argaez], .lightgrey[Diario el tiempo], Enero 2022. 

---
# Intentamos aprender

--

1. Qué es economía?

--

1. Qué .RRed[NO] es la .blue[economía]

--

1. Qué es ".blue[Salud]" (bienestar)

--

1. Qué es economía de la .blue[Salud]?

--

#### Conceptos claves en economía

--

- Costo de oportunidad

--

- Eficiencia


---
# Economía como ciencia trata con...

--

.pull-left[
+ Recursos .RRed[limitados]

+ `Necesidades` ilimitadas

+ Escoger "lo mejor" para 🤗 dada una restricción 💵
]

.pull-right[
<img src="https://media.giphy.com/media/T4SwrRe4yVNvU0dGIY/giphy.gif" width="90%" /> 
]
---
# A modo general

--

+ Qué es **economía**?

--

<ru-blockquote>
Una ciencia social que estudia las decisiones individuales y grupales que se encuentran en un ambiente de escases y de necesidades ilimitadas.</ru-blockquote>

--

+ Qué es **economía de la salud**

--

<md-blockquote>Una rama especializada de la economía que tiene que ver con el estudio de costos, beneficios, localización de recursos, insumos, resultados que tienen que ver con el cuidado de la salud</md-blockquote>

--

+ Dónde surgió esto de economía de la salud?

--

`r fa('caret-right')` Inicio del siglo XX con el AMA Bureau of medical economics. Ademas de .RRed[Milton Friedman] (Premio Nobel en economía) quien miraba la elección optima de recursos. .RRed[Kenneth Arrow] 1963: Uncertainty and the welfare economics of medical care.


---
# Economía es todo sobre elección

--

```{r echo = FALSE}
knitr::include_graphics("images/gh1.png")
```
---
class: inverse, middle

# Concepto Clave 1

--

#### Costo de oportunidad

---
# Costo de oportunidad

--

<md-blockquote>El valor del beneficio perdido que podría obtenerse de un recurso en su siguiente mejor uso alternativo</md-blockquote>

--

**Cuidado Pediatra**|**Cuidado Geriátrico**|**Costo de Oportunidad**
:-----:|:-----:|:-----:
0|30|0
1|28|2
2|18|12
3|14|16
4|10|20
5|0|30

---
# Costo de oportunidad

--

```{r datamed1, include=FALSE}
p <- ppf(x = 4:6, # Intersections
         main = "Frontera de posibilidades",
         geom = "text",
         generic = TRUE, # Generic labels
         labels = c("A", "B", "C"), # Custom labels
         xlab = "Cuidado Pediatrico",
         ylab = "Cuidado Geriatrico",
         acol = 3)      # Color of the area
```

```{r, ex1, echo=FALSE}
p$p + geom_point(data = data.frame(x = 5, y = 5), size = 5) +
  geom_point(data = data.frame(x = 2, y = 2), size = 5) +
  annotate("segment", x = 3.1, xend = 4.25, y = 5, yend = 5,
           arrow = arrow(length = unit(0.5, "lines")), colour = 3, lwd = 1) +
  annotate("segment", x = 4.25, xend = 4.25, y = 5, yend = 4,
           arrow = arrow(length = unit(0.5, "lines")), colour = 3, lwd = 1)
```

---
# Costo de oportunidad

--

`r fa('caret-right', fill="red")` Decidir hacer .blue[A] implica la decisión de NO hacer .blue[B]. (P.e los beneficios de $A>B$)

--

`r fa('caret-right', fill="red")` Los costos ocurren sin necesidad de incurrir en gastos _financieros_.

--

`r fa('caret-right', fill="red")` El "valor" no necesariamente es determinado por el .RRed[Mercado].


---
# La forma en como los economistas ven el mundo

.pull-left[![](https://media.giphy.com/media/3oGRFdJQ6Kz0zhNXuE/giphy.gif)]

.pull-right[`r fa("bacterium", fill="blue")` .lightgrey[Pesimista]: Media botella vacia

`r fa("bacterium", fill="blue")` .lightgrey[Optimista]: Media botella llena

`r fa("bacterium", fill="blue")` .RRed[Economista]: Media botella desperdiciada

<br>
<br>
Eso es ineficiente!!]

---
class: live-code

# Piense esto:

<br>
<br>

### Cuál es el costo de oportunidad que tiene una IPS de camas UCI en comparación de camas de hospitalización?
---
class: middle, center

# `r emo::ji("stopwatch")`

# Es hora de un break!

```{r echo = FALSE}
countdown(minutes = 10, update_every = 15)
```

---
class: your-turn

# <i class="fas fa-cloud"></i>

## Actividad 1: Revise el siguiente enlace

<https://www.youtube.com/watch?v=XBJQENjZJaA&t=1011s&ab_channel=TED>

### Intente responder:

a. Qué le falta a usted para dar la milla extra?

b. Qué mensaje le deja la presentación?

c. Como lo aplicaría a la solución de un problema de su área de estudio.

```{r echo = FALSE}
countdown(minutes = 30, update_every = 35)
```



---
class: inverse, middle

# Concepto Clave 2

--

#### Eficiencia

---
# Eficiencia

--

> Va con el uso, que tan bien se hace uso de los recursos excasos y maximizo una función de bienestar.

--

`r fa("rocket", fill="red")` .RRed[Eficiencia Técnica] Se alcanza un objetivo a un menor "Costo".

--

`r fa("rocket", fill="red")` .RRed[Eficiencia por Asignación] Cuando la oferta iguala a la demanda. Las necesidades de los pacientes son cubiertas por los servicios prestados por todo el sistema de salud.

---
# Tópicos vs Disciplina

--

`r fa("plane", fill="blue")` Tópico $\rightarrow$ área de estudio

--

`r fa("plane", fill="red")` Disciplina $\rightarrow$ Marco conceptual

--

> Economía de la salud es la disciplina (rama) de la economía aplicada a los tópicos de salud.

---
# Algunas ideas erroneas

--

### La economía es...

--

+ Todo lo que tiene que ver con dinero

--

+ Lo mismo que contabilidad

--

+ Solo es practicada por los economistas

--

+ Objetiva

---
# De lo que trata la economía

--

### Economía en cambio si es:

--

`r fa('flask')` Optimización

--

`r fa('flask')` Eficiencia

--

`r fa('flask')` Elección

--

`r fa('flask')` Bienestar

--

### Dinero... 💰

--

+ Deposito de valor

--

+ Medio de intercambio


---
# Qué entonces es salud?

--

La .RRed[Organización Mundial de la Salud] (OMS) la define como un estado de `bienestar` con características que van desde lo físico (movilidad), lo psicológico (mental) y otras consideraciones mas.

--

+ Usualmente ese estado de .grey[salud] va relacionado con una medición o métrica, hay que hacer eso de la _evaluación_ 

---
# Qué entonces es salud?


```{r bte1, echo=FALSE}
knitr::include_graphics("images/gh3.png")
```


---
# Qué entonces es salud?

--

```{r bt1, echo=FALSE}
knitr::include_graphics("images/gh2.png")
```


---
class: middle, center
# Un ejemplo de ese estado

--

`r emo::ji("heavy_check_mark")` **Movilidad:** Como considera que se siente? tiene alguna discapacidad motriz?

--

`r emo::ji("heavy_check_mark")` **Autocuidado:** Cuando fue la última vez que se automedico? Por qué lo hizo? 

--

`r emo::ji("heavy_check_mark")` **Actividades cotidianas** Qué hace regularmente en su trabajo? hace deporte? cuantas horas a la semana lo hace?

--

`r emo::ji("heavy_check_mark")` **Dolor/ inconformidad:** ha presentado algún dolor en la última semana?

--

`r emo::ji("heavy_check_mark")` **Ansiedad:** Ha tenido algún evento que ha padecido un episodio de ansiedad no controlada?

---
# Bibliografía

`r fa('book')` Notas de clase de Health economics Smith & Wright 

`r fa('book')` Cantarero, D., Lanza, P., & Lera, J. (2020). Juan Oliva Moreno, Beatriz González López-Valcárcel, Marta Trapero Bertrán, Álvaro Hidalgo Vega y Juan E. del Llano Señarís. Economía de la salud. Madrid: Pirámide; 2018. 444 p. ISBN: 978-84-368-3977-7. Gaceta Sanitaria, 34, 98-98.

`r fa('book')` Cabasés, J. M., & Oliva, J. (2011). El gobierno de la sanidad frente a la crisis económica. Notas sobre economía de la salud y sostenibilidad del sistema sanitario. Cuadernos de información económica, 225, 57-62.

`r fa('book')`Phelps, C. E. (2017). Health economics. Routledge.

---
class: your-turn

## Gracias por su atención!

### Alguna pregunta adicional?

#### Carlos Andres Yanes Guerra
`r fa("envelope", fill="red")` cayanes@uninorte.edu.co
`r fa("twitter", fill="cyan")` keynes37





